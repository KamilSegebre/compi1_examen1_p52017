#include <iostream>
#include "lexic.h"
#include "syntax.h"

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        std::cout << "Need 1 input." << std::endl;
        return -1;
    }

    std::ifstream input_file(argv[1]);
    
    if(!input_file.is_open())
    {
        std::cout << "File not found." << std::endl;
        return -1;
    }

    Lexic lexer(&input_file);
    Syntax syntax(&lexer);
    try
    {
        syntax.parse();
    }
    catch (std::string s)
    {
        std::cout << s << std::endl;
        return -1;
    }
    std::cout << "All good" << std::endl;
   return 0;
}