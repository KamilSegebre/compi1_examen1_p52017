#include "syntax.h"

void Syntax::parse()
{
    A();
}

void Syntax::A()
{
    if (current_token == Token::TK_INPUT)
    {   
        if (update_token() == Token::TK_ID)
        {
            B();
            if (update_token() == Token::TK_SEMI_COLON)
            {
                C();
                return;
            }
            else
            {
                std::string s("Syntax Error. Expected ;");
                throw s;
            }
        }
        else
        {
            std::string s("Syntax Error. Expected id");
            throw s;
        }
    }
    else
    {
        std::string s("Syntax Error. Expected 'input'");
        throw s;
    }
}

void Syntax::B()
{
    if (update_token() == Token::TK_COMA)
    {
        if (update_token() == Token::TK_ID)
        {
            B();
        }
        else
        {
            std::string s("Syntax Error. Expected id");
            throw s;
        }
    }
    else
    {
        return;
    }
}

void Syntax::C()
{
    if (update_token() == Token::TK_OUTPUT)
    {
        if (update_token() == Token::TK_ID)
        {
            B();
            if (update_token() == Token::TK_SEMI_COLON)
            {
                D();
                return;
            }
            else
            {
                std::string s("Syntax Error. Expected ;");
                throw s;
            }
        }
        else
        {
            std::string s("Syntax Error. Expected id");
            throw s;
        }
    }
    else
    {
        std::string s("Syntax Error. Expected 'output'");
        throw s;
    }
}

void Syntax::D()
{
    if (update_token() == Token::TK_BEGIN)
    {
        if (update_token() == Token::TK_TABLE)
        {
            if (update_token() == Token::TK_BRACKET_L)
            {
                if (update_token() == Token::TK_ID)
                {
                    B();
                    if (update_token() == Token::TK_BRACKET_R)
                    {
                        if (update_token() == Token::TK_COMA)
                        {
                            E();
                            if (update_token() == Token::TK_END)
                            {
                                if (update_token() == Token::TK_EOF)
                                {
                                    return;
                                }
                                else
                                {
                                    std::string s("Syntax Error");
                                    throw s;
                                }
                            }
                            else
                            {
                                std::string s("Syntax Error");
                                throw s;
                            }
                        }
                        else
                        {
                            std::string s("Syntax Error");
                            throw s;
                        }
                    }
                    else
                    {
                        std::string s("Syntax Error");
                        throw s;
                    }
                }
                else
                {
                    std::string s("Syntax Error");
                    throw s;
                }
            }
            else
            {
                std::string s("Syntax Error");
                throw s;
            }
        }
        else
        {
            std::string s("Syntax Error");
            throw s;
        }
    }
    else
    {
        std::string s("Syntax Error");
        throw s;
    }
}

void Syntax::E()
{
    F();
    if (update_token() == Token::TK_ARROW)
    {
        F();
        H();
    }
    else
    {
        std::string s("Syntax Error");
        throw s;
    }
}

void Syntax::F()
{
    Token t = update_token();
    if (t == Token::TK_ONE)
    {
        G();
    }
    else if(t == Token::TK_ZERO)
    {
        G();
    }
    else
    {
        std::string s("Syntax Error");
        throw s;
    }
}

void Syntax::G()
{
    Token t = update_token();
    if (t == Token::TK_ONE)
    {
        G();
    }
    else if (t == Token::TK_ZERO)
    {
        G();
    }
    else
    {
        return;
    }
}

void Syntax::H()
{
    if (update_token() == Token::TK_COMA)
    {
        E();
    }
    else
    {
        std::string s("Syntax Error");
        throw s;
    }
}