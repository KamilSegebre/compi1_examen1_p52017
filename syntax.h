#include "lexic.h"

class Syntax
{
public:
    Syntax(Lexic *lexer) : lexer(lexer)
    {
        update_token();
    }
    
    void parse();

private:
    Token update_token()
    {
        current_token = lexer->get_next_token();
        return current_token;
    }
    void A();
    void B();
    void C();
    void D();
    void E();
    void F();
    void G();
    void H();

private:
    Lexic *lexer;
    Token current_token;
};