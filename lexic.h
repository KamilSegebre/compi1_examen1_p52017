#ifndef __LEXIC_H
#define __LEXIC_H

#include <fstream>
#include <string>

enum class Token {
    TK_INPUT = 1,
    TK_OUTPUT = 2,
    TK_ID = 3,
    TK_BEGIN = 4,
    TK_TABLE = 5,
    TK_BRACKET_L = 6,
    TK_BRACKET_R = 7,
    TK_ARROW = 8,
    TK_ONE = 9,
    TK_ZERO = 10,
    TK_COMA = 11,
    TK_END = 12,
    TK_SEMI_COLON = 13,
    TK_EOF = 14,
};

class Lexic {
public:
    Lexic(std::ifstream *input_file) : input_file(input_file)
    {
        get_next_symbol();
    }

    Token get_next_token();

private:
    char get_next_symbol()
    {
        current_symbol = input_file->get();
        return current_symbol;
    }

private:
    std::ifstream *input_file;
    char current_symbol;
};

#endif