#include "lexic.h"
#include <iostream>
Token Lexic::get_next_token()
{
    for (;;)
    {
        if (current_symbol == ' ' || current_symbol == '\n' || current_symbol == '\t')
            get_next_symbol();
        else
        {
            switch (current_symbol)
            {
            case '[':
                get_next_symbol();
                return Token::TK_BRACKET_L;
            case ']':
                get_next_symbol();
                return Token::TK_BRACKET_R;
            case ',':
                get_next_symbol();
                return Token::TK_COMA;
            case ';':
                get_next_symbol();
                return Token::TK_SEMI_COLON;
            case '0':
                get_next_symbol();
                return Token::TK_ZERO;
            case '1':
                get_next_symbol();
                return Token::TK_ONE;
            case '=':
                if (get_next_symbol() == '>')
                {
                    get_next_symbol();
                    return Token::TK_ARROW;
                }
                else
                {
                    std::string s("Lexer Error. Expected '>' but found something else.");
                    throw s;
                }
            case '/':
                if (get_next_symbol() == '/')
                    while (get_next_symbol() != '\n' && !input_file->eof())
                    {
                        break;
                    }
                else
                {
                    std::string s("Lexer Error. Random '/' found.");
                    throw s;
                }
                break;
            default:
                //If is letter or '_'
                if ((current_symbol >= 'a' && current_symbol <= 'z') || (current_symbol >= 'A' && current_symbol <= 'Z') || current_symbol == '_')
                {
                    if (current_symbol == 'i')
                    {
                        if (get_next_symbol() == 'n')
                            if (get_next_symbol() == 'p')
                                if (get_next_symbol() == 'u')
                                    if (get_next_symbol() == 't')
                                    {
                                        get_next_symbol();
                                        return Token::TK_INPUT;
                                    }
                    }
                    else if (current_symbol == 'o')
                    {
                        if (get_next_symbol() == 'u')
                            if (get_next_symbol() == 't')
                                if (get_next_symbol() == 'p')
                                    if (get_next_symbol() == 'u')
                                        if (get_next_symbol() == 't')
                                        {
                                            get_next_symbol();
                                            return Token::TK_OUTPUT;
                                        }
                    }
                    else if (current_symbol == 't')
                    {
                        if (get_next_symbol() == 'a')
                            if (get_next_symbol() == 'b')
                                if (get_next_symbol() == 'l')
                                    if (get_next_symbol() == 'e')
                                    {
                                        get_next_symbol();
                                        return Token::TK_TABLE;
                                    }
                    }
                    else if (current_symbol == 'b')
                    {
                        if (get_next_symbol() == 'e')
                            if (get_next_symbol() == 'g')
                                if (get_next_symbol() == 'i')
                                    if (get_next_symbol() == 'n')
                                    {
                                        get_next_symbol();
                                        return Token::TK_BEGIN;
                                    }
                    }
                    else if (current_symbol == 'e')
                    {
                        if (get_next_symbol() == 'n')
                            if (get_next_symbol() == 'd')
                            {
                                get_next_symbol();
                                return Token::TK_END;
                            }
                    }

                    //While its a letter, '_', or number
                    while ((current_symbol >= 'a' && current_symbol <= 'z') || (current_symbol >= 'A' && current_symbol <= 'Z') || (current_symbol == '_') || (current_symbol >= '0' && current_symbol <= '9'))
                    {
                        if (input_file->eof())
                            break;
                        get_next_symbol();
                    }

                    return Token::TK_ID;
                }
                else if (input_file->eof())
                {
                    return Token::TK_EOF;
                }
                else
                {
                    std::string s("Lexer Error. Unexpected Symbol Found");
                    throw s;
                }
            }
        }
    }
}